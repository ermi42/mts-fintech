package task2;

import task2.part1.Circle;
import task2.part1.Point;
import task2.part1.Triangle;
import task2.part2.Polygon;
import task2.part2.Rhombus;

public class Main {
    public static void main(String[] args) {
        testCircle();
        System.out.println("----------------------------------");
        testTriangle();
        System.out.println("----------------------------------");
        testPolygon();
        System.out.println("----------------------------------");
        testRhombus();
    }

    static void testCircle() {
        System.out.println("\nСоздаю круг радиусом 1 и центром в точке (10;0) черного цвета:");
        Circle circle1 = new Circle(10, 0, 1, "black");
        System.out.println(circle1);
        System.out.println("\nУстанавливую новые параметры круга и вывожу информацию о нем:");
        circle1.setRadius(10);
        circle1.setCenter(new Point(0, 0));
        circle1.setColor("white");
        System.out.println(circle1);
        System.out.println("\nВывожу только цвет круга:");
        System.out.println(circle1.getColor());
        System.out.println("\nВывожу периметр и площадь:");
        System.out.println(circle1.GetPerimeter());
        System.out.println(circle1.GetArea());
    }

    static void testTriangle() {
        System.out.println("\nСоздаю треугольник оранжевого цвета");
        Triangle triangle1 = new Triangle(1, 1, 2, 4, 4, 2, "orange");
        System.out.println(triangle1);
        System.out.println("\nУстанавливую новые параметры треугольника и вывожу информацию о нем:");
        triangle1.setA(new Point(-4, 0));
        triangle1.setB(new Point(-1, -4));
        triangle1.setC(new Point(4, 6));
        triangle1.setColor("green");
        System.out.println(triangle1);
        System.out.println("\nВывожу только цвет треугольника:");
        System.out.println(triangle1.getColor());
        System.out.println("\nВывожу периметр и площадь:");
        System.out.println(triangle1.GetPerimeter());
        System.out.println(triangle1.GetArea());
        System.out.println("\nВывожу его координаты:");
        triangle1.output();
    }

    static void testPolygon() {
        System.out.println("\nСоздаю полигон с точками (1,1), (5,6), (10,6), (10,1) желтого цвета:");

        Polygon polygon1 = new Polygon(4, "yellow", 1, 1, 5, 6, 10, 6, 10, 1);
        System.out.println(polygon1);
        System.out.println("\nВывожу периметр и площадь:");
        System.out.println(polygon1.GetPerimeter());
        System.out.println(polygon1.GetArea());
        System.out.println("\nЗадаю другой цвет полигона -  красный:");
        polygon1.setColor("red");
        System.out.println(polygon1);
        System.out.println("\nВывожу только цвет полигона:");
        System.out.println(polygon1.getColor());
        System.out.println("\nВывожу его координаты:");
        polygon1.getCoordinates();
    }

    static void testRhombus() {
        System.out.println("\nСоздаю ромб с координатами в точках (-4, 0) (0, 6) (4, 0) (0, -6) желтого цвета:");
        Rhombus rhombus1 = new Rhombus(4, "yellow", -4, 0, 0, 6, 4, 0, 0, -6);
        System.out.println(rhombus1);
        System.out.println("\nВывожу длины наименьшей и наибольшей диагоналей:");
        System.out.println(rhombus1.getSmallestDiagonal());
        System.out.println(rhombus1.getLongestDiagonal());
        System.out.println("\nВывожу периметр и площадь через координаты:");
        System.out.println(rhombus1.GetPerimeter());
        System.out.println(rhombus1.GetArea());
        System.out.println("\nЗадаю другой цвет ромба - белый:");
        rhombus1.setColor("white");
        System.out.println(rhombus1);
        System.out.println("\nВывожу только цвет ромба:");
        System.out.println(rhombus1.getColor());
        System.out.println("\nВывожу его координаты:");
        rhombus1.getCoordinates();
    }
}
