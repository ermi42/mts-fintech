package task2.part2;

public class Rhombus extends Polygon implements Diagonal {
    public Rhombus(int count, String color, double... coordinates) {
        super(count, color, coordinates);
    }

    @Override
    public double getSmallestDiagonal() {
        return Math.min(points.get(0).getLength(points.get(2)), points.get(1).getLength(points.get(3)));
    }

    @Override
    public double getLongestDiagonal() {
        return Math.max(points.get(0).getLength(points.get(2)), points.get(1).getLength(points.get(3)));
    }
}
