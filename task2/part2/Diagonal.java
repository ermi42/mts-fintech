package task2.part2;

public interface Diagonal {
    double getSmallestDiagonal();
    double getLongestDiagonal();
}
