package task2.part2;

import task2.part1.GeomFigure;
import task2.part1.Point;

import java.util.ArrayList;
import java.util.List;

public class Polygon extends GeomFigure implements WithAngles {
    private final int count; // кол-во вершин
    protected List<Point> points = new ArrayList<>();
    private String color;

    public Polygon(int count, String color, double... coordinates) {
        this.color = color;
        this.count = count;
        for (int i = 0; i < count * 2; i += 2) {
            points.add(new Point(coordinates[i], coordinates[i + 1]));
        }
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public double GetPerimeter() {
        double result = 0;
        Point currentPoint;
        result += points.get(0).getLength(points.get(count - 1));
        for (int i = 0; i < count - 1; i++) {
            currentPoint = points.get(i);
            result += currentPoint.getLength(points.get(i + 1));
        }
        return result;
    }

    // считаем по формуле Гаусса
    // 1/2 * |xi*y(i+1) + xn*y1 - x(i+1)*yi - x1yn|
    @Override
    public double GetArea() {
        double tempSum1 = 0;
        double tempSum2 = 0;
        for (int i = 0; i < count; i++) {
            tempSum1 += points.get(i).getX() * points.get((i + 1) % count).getY();
            tempSum2 += points.get((i + 1) % count).getX() * points.get(i).getY();
        }
        return 0.5 * Math.abs(tempSum1 - tempSum2);
    }

    @Override
    public void getCoordinates() {
        Point currentPoint;
        for (int i = 0; i < count; i++) {
            currentPoint = points.get(i);
            System.out.printf("Point №%d: (%f, %f)\n", i + 1, currentPoint.getX(), currentPoint.getY());
        }
    }

    @Override
    public String toString() {
        Point currentPoint;
        StringBuilder output = new StringBuilder("Polygon: Color " + color + "\n");
        for (int i = 0; i < count; i++) {
            currentPoint = points.get(i);
            output.append(String.format("Point №%d: (%f, %f)\n", i + 1, currentPoint.getX(), currentPoint.getY()));
        }
        return output.toString();
    }
}
