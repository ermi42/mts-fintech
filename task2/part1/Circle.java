package task2.part1;

public class Circle extends GeomFigure {
    private Point center;
    private double radius;
    private String color;

    public Circle(double centerX, double centerY, double radius, String color) {
        center = new Point(centerX, centerY);
        this.radius = radius;
        this.color = color;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public double GetPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double GetArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return String.format("Круг с цветом %s\nКоординаты центра в точке (%f, %f)\nРадиус = %f",
                color, center.getX(), center.getY(), radius);
    }
}
