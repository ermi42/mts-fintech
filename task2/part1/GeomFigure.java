package task2.part1;

public abstract class GeomFigure {
    public abstract double GetPerimeter();

    public abstract double GetArea();
}
