package task2.part1;

public class Point {
    private final double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getLength(Point p) {
        return Math.pow(Math.pow((x - p.getX()), 2) + Math.pow((y - p.getY()), 2), 0.5);
    }
}
