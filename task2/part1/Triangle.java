package task2.part1;

public class Triangle extends GeomFigure {
    private Point a, b, c;
    private String color;

    public Triangle(double ax, double ay, double bx, double by, double cx, double cy, String color) {
        a = new Point(ax, ay);
        b = new Point(bx, by);
        c = new Point(cx, cy);
        this.color = color;
    }

    public void setA(Point a) {
        this.a = a;
    }

    public void setB(Point b) {
        this.b = b;
    }

    public void setC(Point c) {
        this.c = c;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void output() {
        System.out.println(a.getX() + " " + a.getY());
        System.out.println(b.getX() + " " + b.getY());
        System.out.println(c.getX() + " " + c.getY());
    }

    @Override
    public double GetPerimeter() {
        double ab, ac, bc;
        ab = a.getLength(b);
        ac = a.getLength(c);
        bc = b.getLength(c);
        return ab + ac + bc;
    }

    @Override
    public double GetArea() {
        double ab, ac, bc;
        ab = a.getLength(b);
        ac = a.getLength(c);
        bc = b.getLength(c);
        // воспользуемся формулой Герона
        double p = GetPerimeter() / 2;
        return Math.pow((p * (p - ab) * (p - ac) * (p - bc)), 0.5);
    }

    @Override
    public String toString() {
        return String.format("Треугольник с цветом %s\nИмеет вершины в точках (%f,%f) (%f,%f), (%f,%f)",
                color, a.getX(), a.getY(), b.getX(), b.getY(), c.getX(), c.getY());
    }
}
